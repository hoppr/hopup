/*
 *  File: root.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package cmd

import (
	"fmt"
	"os"

	"github.com/golang-demos/chalk"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/hoppr/droppr/pkg/configs"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "droppr",
	Short: "Droppr: A tool for unbundling hoppr bundles.",
	Long: `Droppr: A tool for unbundling hoppr bundles.
	 
 Providing a simple mechanism to distribute artifacts in a Hoppr bundle to target registries and repositories.
	 `,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is .droppr.yml or $HOME/.config/.droppr.yml)")
	rootCmd.PersistentFlags().Int("num_workers", 10, "set number of worker threads for processing")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	rootCmd.PersistentFlags().BoolP("debug", "d", false, "Toggle Debug Logging")

	rootCmd.PersistentFlags().StringP("bundle", "b", "", "Hoppr bundle to process")
	rootCmd.PersistentFlags().StringP("logfile", "l", "droppr.log", "Log file location")

}

// initConfig reads in config file and ENV variables if set.
func initConfig() (*configs.DropprConfig, error) {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".droppr" (without extension).
		viper.AddConfigPath(".")
		viper.AddConfigPath(home + "/.config")
		viper.SetConfigType("yaml")
		viper.SetConfigName(".droppr.yml")
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err != nil {
		log.Print(err)
		return nil, err
	}

	// Unmarshal config file contents into the DropprConfig struct
	var config = new(configs.DropprConfig)
	if err := viper.UnmarshalExact(config); err != nil {
		log.Print(err)
		return nil, err
	}

	// Fill in basic defaults
	if err := config.FillInConfigDefaults(); err != nil {
		errorMsg := chalk.Red(fmt.Sprintf("ERROR: Failed to fill in config defaults: %s", err)).String()
		fmt.Println(errorMsg)
		return nil, err
	}

	// Look up password_env values
	if err := FillPassEnvConfigs(config); err != nil {
		errorMsg := chalk.Red(fmt.Sprintf("ERROR: Failed to process one or more configuration(s): %s", err)).String()
		fmt.Println(errorMsg)
		os.Exit(5)
	}

	// get number of parallel workers
	config.Num_Workers = GetNumWorkers(rootCmd, config)
	return config, nil
}

// Look up all password env values
func FillPassEnvConfigs(inPkg *configs.DropprConfig) error {
	errors_found := false

	for indx := range inPkg.Repos {

		if err := FillPassEnvConfig(&inPkg.Repos[indx]); err != nil {
			errors_found = true
		}
	}
	if errors_found {
		return fmt.Errorf("fill password for configuration(s)")
	}

	return nil
}

// Look up single password env value
func FillPassEnvConfig(r *configs.PackageType) error {
	if r.Password_Env != "" {
		r.Password = os.Getenv(r.Password_Env)
		if r.Password == "" {
			errorMsg := chalk.Red(fmt.Sprintf("ERROR: Unable to read environment variable %s for %s %s install.",
				r.Password_Env, r.Purl_Type, r.Target_Type)).String()
			fmt.Println(errorMsg)
			return fmt.Errorf("failed to read password env var %s", r.Password_Env)
		}
	}
	return nil

}

func GetNumWorkers(cmd *cobra.Command, config *configs.DropprConfig) int {
	var num_Workers int
	if cmd.Flags().Changed("num_workers") {
		num_Workers, _ = cmd.Flags().GetInt("num_workers")
		log.Printf("Worker thread count set to %d on the command line", num_Workers)
	} else if config != nil && config.Num_Workers != 0 {
		num_Workers = config.Num_Workers
		log.Printf("Workers thread count set to %d, from config file", num_Workers)
	} else {
		num_Workers = 10
		log.Printf("Worker thread count not specified, defaulting to %d", num_Workers)
	}

	if num_Workers <= 0 {
		log.Print("Error: Number of workers cannot be negative or 0")
		fmt.Printf("Error: Number of workers cannot be negative or 0")
		os.Exit(1)
	}

	return num_Workers
}
