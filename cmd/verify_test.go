/*
 *  File: verify_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package cmd

import (
	"fmt"
	"io"
	"os"
	"testing"

	"github.com/spf13/cobra"
	"github.com/stretchr/testify/assert"
)

func TestRunVerifyCommand(t *testing.T) {
	callCount := 0
	testCases := map[string]struct {
		bundleName     string
		logfileName    string
		openError      bool
		logInitialized bool
		expectedError  error
	}{
		"no bundle":   {"", "logfile.txt", false, false, fmt.Errorf("Error: Hoppr bundle not specified\n")},
		"bad logfile": {"bundle.tar.gz", "errorfile.bin", true, false, fmt.Errorf("Failed to open log file errorfile.bin: Failed to open log file\n")},
		"init log":    {"bundle.tar.gz", "logfile.txt", false, true, nil},
	}
	mockInitLog := func(io.Writer) {
		callCount++
	}
	var cmd *cobra.Command = new(cobra.Command)
	cmd.Flags().StringP("bundle", "b", "", "Hoppr bundle to process")
	cmd.Flags().StringP("logfile", "l", "default.log", "logfile")

	for key, tc := range testCases {
		var err error
		callCount = 0
		err = cmd.Flags().Set("bundle", tc.bundleName)
		assert.True(t, err == nil, "Failed to add test flag --bundle")
		err = cmd.Flags().Set("logfile", tc.logfileName)
		assert.True(t, err == nil, "Failed to add test flag --logfile")

		mockOpenLog := func(name string) (*os.File, error) {
			if tc.openError {
				return (*os.File)(nil), fmt.Errorf("Failed to open log file")
			} else {
				return new(os.File), nil
			}
		}

		err = runVerifyCommand(cmd, nil, mockOpenLog, mockInitLog)
		assert.Equal(t, tc.expectedError, err, "Error mismatch on test "+key)
		assert.Equal(t, tc.logInitialized, (callCount == 1), "logging Initialize not called")

	}

}
