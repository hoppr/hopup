/*
 *  File: install.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	log "github.com/sirupsen/logrus"

	"github.com/spf13/viper"
	"gitlab.com/hoppr/droppr/pkg/extractors"
	"gitlab.com/hoppr/droppr/pkg/install"
	"gitlab.com/hoppr/droppr/pkg/logging"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

// installCmd represents the install command
var installCmd = &cobra.Command{
	Use:   "install -b bundleFilename",
	Short: "Installs artifacts from a Hoppr bundle",
	Long: `Unpacks and installs artifacts from a Hoppr bundle to delivery locations
according to PURL type(s) and target delivery location(s) specified in the Droppr configuration file. 

See Droppr documentation in https://hoppr.dev`,
	Run: runInstallCommand,
}

func init() {
	// Add this command to its parent's subcommand list
	rootCmd.AddCommand(installCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// installCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// installCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

}

// implements the install command high level processing
func runInstallCommand(cmd *cobra.Command, args []string) {

	hopprBundle, _ := cmd.Flags().GetString("bundle")
	if hopprBundle == "" {
		fmt.Fprintf(os.Stderr, "Error: Hoppr bundle not specified\n")
		os.Exit(1)
	}
	fmt.Printf("Beginning install process on bundle \"%s\"\n", hopprBundle)

	logfileName, _ := cmd.Flags().GetString("logfile")
	logFile, err := utils.OpenLogfile(logfileName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open log file %s: %s\n", logfileName, err.Error())
		os.Exit(4)
	}
	defer logFile.Close()

	logging.Initialize(logFile)

	config, err := initConfig()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading config file %s: %s\n", viper.ConfigFileUsed(), err.Error())
		os.Exit(1)
	}

	var extractedDir string

	// If an an existing directory with an extracted bundle is provided, use it
	if config.Existing_Directory != "" {
		_, err := os.Stat(config.Existing_Directory)
		if err != nil {
			log.Errorf("Given directory does not exist: %s\n", err.Error())
			fmt.Println("Given directory does not exist: ", err)
			os.Exit(6)
		}
		extractedDir = config.Existing_Directory
	} else {
		extractedDir, err = os.MkdirTemp(config.Unpack_Directory, "hoppr_bundle_")
		if err != nil {
			log.Errorf("Error creating unpack directory: %s\n", err.Error())
			fmt.Println("Error creating unpack directory: ", err)
			os.Exit(5)
		}

		if !config.Persist_Extract_Directory {
			defer os.RemoveAll(extractedDir)
		} else {
			defer fmt.Println("Extracted Hoppr bundle contents to directory: ", extractedDir)
		}

		if err := extractors.UnpackHopprArtifact(extractedDir, hopprBundle); err != nil {
			log.Errorf("Error return unpacking Hoppr Artifact %s: %s\n", hopprBundle, err.Error())
			fmt.Println("Error return unpacking Hoppr Artifact ", hopprBundle, ": ", err)
			os.Exit(2)
		}
		log.Println("Successfully extracted Hoppr bundle ", hopprBundle)
	}

	if err := install.Distribute(extractedDir, config); err != nil {
		log.Errorf("Error return distributing packages from %s: %s\n", hopprBundle, err.Error())
		os.Exit(3)
	}

}
