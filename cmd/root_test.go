/*
 *  File: root_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package cmd

import (
	"errors"
	"testing"

	"github.com/spf13/cobra"
	"gitlab.com/hoppr/droppr/pkg/configs"
)

func TestGetNumWorkers(t *testing.T) {
	var standardDropprConfig = new(configs.DropprConfig)

	testCases := map[string]struct {
		config     *configs.DropprConfig
		numWorkers string
		want       int
		wantErr    error
	}{
		"defaultToTen": {standardDropprConfig, "", 10, errors.New("Error: Number of workers cannot be negative or 0")},
		"fiveWorkers":  {standardDropprConfig, "5", 5, nil},
	}

	for _, tc := range testCases {
		var command *cobra.Command = new(cobra.Command)
		command.Flags().Int("num_workers", 0, "num_workers")
		err := command.Flags().Set("num_workers", tc.numWorkers)
		if err != nil {
			t.Log(err)
		}

		if got := GetNumWorkers(command, tc.config); got != tc.want {
			t.Errorf("GetNumWorkers() = %v, want %v", got, tc.want)
		}
	}

}

func TestFillPassEnvConfig(t *testing.T) {
	tests := []struct {
		name    string
		passenv string
		wantErr bool
	}{
		{"envVarSetButDoesntExist", "MYAWESOMEVAR", true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var pkg *configs.PackageType = new(configs.PackageType)
			pkg.Password_Env = tt.passenv

			if err := FillPassEnvConfig(pkg); (err != nil) != tt.wantErr {
				t.Errorf("FillPassEnvConfig() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
