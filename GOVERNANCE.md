# Governance

## Principles

The Lockheed Martin Open Source Community adheres to the following principles:

* Open: Lockheed Martin Open Source Projects are intended for public use and are considered a public good
* Welcoming and respectful: See [Code of Conduct](https://hoppr.dev/docs/development/contributing/guide)
* Patience: Expect to move slower together than fast alone
* Transparent and accessible: Work and collaboration should be done in public. Please create issues and use community channels to discuss contributions before implementation
* Merit: Ideas and contributions are accepted according to their technical merit and alignment with project objectives, scope, design principles, and most importantly stability for existing users

## Code of Conduct

__The Lockheed Martin Open Source Community abides by this project's [Code of Conduct](https://hoppr.dev/docs/development/contributing/guide).__

As contributors and maintainers of this project, and in the interest of fostering an open and welcoming community, we pledge to respect all people who contribute through reporting issues, posting feature requests, updating documentation, submitting pull requests or patches, and other activities.

As a member of this project, you represent the project and your fellow contributors. We value our community tremendously and we'd like to keep cultivating a friendly and collaborative environment for our contributors and users. We want everyone in the community to have positive experiences.

## Governance Model

Droppr uses a __hierarchical__ decision-making process. While similar to [Hoppr's tiers](https://hoppr.dev/docs/development/contributing/governance), Droppr only has the `OSPO`, `Maintainers`, and `Contributors` tiers.

```mermaid
graph TD
OSPO["OSPO"]
MAIN["Maintainers"]
CON["Contributors"]
OSPO <--> |"Feedback on Governance, Compliance, Community Management, and Selection of Maintainers"| MAIN
MAIN <--> |"Feedback on Implementation, Vision, Community, and Technical Decisions"| CON
```

### Contributor Tier

This project's __contribuors__, who are made of the greater open source community, are expected to actively engage with the project's __maintainers__ before starting implementation. They are expected to think about how changes will affect stability and ensure no Business Area or program would be excluded from the contributed change.

__Contributors__ are expected to meet the quality, testing, and documentation standards of this project. If a __contributor__ changes break functionality for other users, the __contributor__ is responsible for implementing a fix or "rolling back" the change. Failure to do so may be considered a violation of the [Code of Conduct](https://hoppr.dev/docs/development/contributing/guide). __Contributors__ should be aware of the risk associated with contributing changes and of the potential additional charging to address breaking functionality.

__Responsibilities__:

* Communicating desired changes and building consensus with the `maintainers` before starting work
* Contributing changes associated with features or fixes that are discussed with project `maintainers`
* Resolving any functionality breakages introduced by their changes
* Respecting the implementation decisions of the `maintainers`
* Being patient with other `contributors`, `maintainers`, and `Lockheed Martin Open Source Program Office` who have multiple roles and responsibilities

### Maintainers Tier

This project's __maintainers__, who are made of Lockheed Martin employees, are expected to triage issues and feature requests. There is no expectation that maintainers are required to implement fixes or features, but should provide feedback and technical assessment. It is important for the community to actively contribute their desires or report issues without the expectation of others to implement on their behalf.

__Maintainers__ have autonomy to decide bug fixes and feature implementation, but are expected to prioritize product stability over feature changes that would break functionality for existing users. When disputes or deviance arise in vision, community, and technical implementation, the __maintainers__ are expected to engage with each other and the __OSPO__ to resolve.

__Responsibilities__:

* Establishing a welcoming and transparent community
* Triaging issues and feature requests
* Communicating concerns in vision, community, and technical implementation to the community
* Arbitrator for vision, community, and technical decisions when requested by the community

#### Selection Of New Maintainers

New __maintainers__ are selected by the the existing __maintainers__ and confirmed by the __Lockheed Martin Open Source Program Office__. New __maintainers__ should not be granted permissions without agreement from the __Lockheed Martin Open Source Program Office__.

The existing __maintainers__ will select new __maintainers__ based on the following criteria.

* Lockheed Martin employment
* Based on need to have multiple Business Areas involved
* Proven record of stable contributions and involvement in this project
* Following this project's [Code of Conduct](https://hoppr.dev/docs/development/contributing/guide)

#### Retiring Maintainers

If a __maintainer__ is not consistently meeting this criteria or deemed inactive, the __Lockheed Martin Open Source Program Office__
may replace or retire a __maintainer__. If a replacement is needed to ensure multiple Business Areas are involved, the
existing __maintainers__ and __Lockheed Martin Open Source Program Office__ will follow the [Selection Of New Maintainers](#selection-of-new-maintainers)
criteria.

### Open Source Program Office (OSPO) Tier

The Lookheed Martin Open Source Program Office, are the final arbitrators for project ownership, governance, and compliance.

__Responsibilities__:

* Arbitrator for for project ownership, governance, and compliance when requested by the community
* Monitor project for compliance to Lockeed Martin standards
* Ensuring at least 3 maintainers are actively engaged in the project
