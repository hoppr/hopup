/*
 *  File: git.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package git

import (
	"os"
	"strings"

	"gitlab.com/hoppr/droppr/pkg/utils"
)

type Repository struct {
	TargetBaseDir   string
	TargetDirectory string
	Command         []string
	RemoteDir       string
	Version         string
}

// set local copy so its available in namespace for unit tests
var runCommandInDir = utils.RunCommandIn

func (self *Repository) appendToGitCommand(CommandArgs []string) (string, error) {
	fullCommand := append(self.Command, CommandArgs...)
	output, err := runCommandInDir(fullCommand, self.TargetDirectory)
	return output, err
}

func (self *Repository) IsShallowClone() (bool, error) {
	output, err := self.appendToGitCommand([]string{"rev-parse", "--is-shallow-repository"})
	isShallowGitClone := (strings.TrimSpace(string(output)) == "true")
	return isShallowGitClone, err
}

func (self *Repository) AddRemote() (string, error) {
	output, err := self.appendToGitCommand([]string{"remote", "add", "droppr", self.RemoteDir})
	return output, err
}

func (self *Repository) PushAllBranchesToRemote() (string, error) {
	output, err := self.appendToGitCommand([]string{"push", "droppr", "--tags", "refs/remotes/origin/*:refs/heads/*"})
	return output, err
}

// returns revision if Tag is present
func (self *Repository) CheckForTag() (string, error) {
	tagRevision, err := self.appendToGitCommand([]string{"rev-parse", "--verify", "refs/tags/" + self.Version})
	return tagRevision, err
}

// Push Version To Branch
func (self *Repository) PushBranch() (string, error) {
	return self.appendToGitCommand([]string{"push", "droppr", self.Version})
}

// creates branch, pushes branch, and pushes tag
func (self *Repository) PushTag() (string, error) {
	// create branch like "tag-vX.X.X"
	branchOutput, branchErr := self.appendToGitCommand([]string{"checkout", "-f", "-b", "tag-" + self.Version})
	if branchErr != nil {
		return branchOutput, branchErr
	}

	// push created branch to remote
	pushBranchOutput, pushBranchErr := self.appendToGitCommand([]string{"push", "droppr"})
	if pushBranchErr != nil {
		return pushBranchOutput, pushBranchErr
	}

	// push tag to remote
	return self.appendToGitCommand([]string{"push", "droppr", "tag", self.Version})
}

// returns revision if branch is present
func (self *Repository) CheckForBranch() (bool, error) {
	branchRevision, err := self.appendToGitCommand([]string{"rev-parse", "--verify", "origin/" + self.Version})
	if branchRevision != "" && err == nil {
		return true, err
	}
	return false, err
}

func (self *Repository) PushVersionToRemote() (string, error) {
	hasBranchWithVersion, err := self.CheckForBranch()
	if hasBranchWithVersion && err == nil {
		return self.PushBranch()
	}

	tagRevision, err := self.CheckForTag()
	if tagRevision != "" && err == nil {
		return self.PushTag()
	}

	return self.PushAllBranchesToRemote()
}

func (self *Repository) CleanUp() error {
	return os.RemoveAll(self.TargetBaseDir)
}
