/*
 *  File: git_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package git

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func runCmdInDirTrueNoErr(command []string, dir string) (string, error) {
	return "true", nil
}

func runCmdInDirFalseNoErr(command []string, dir string) (string, error) {
	return "false", nil
}

func runCmdInDirEmptyOutputNoErr(command []string, dir string) (string, error) {
	return "", nil
}

func TestIsShallowClone(t *testing.T) {
	testCases := map[string]struct {
		repo         Repository
		stubFunction func(command []string, dir string) (string, error)
		wantOutput   bool
		wantErr      error
	}{
		"IsNotShallowClone": {
			repo: Repository{
				Command:         []string{"git"},
				RemoteDir:       "./unbundle",
				TargetDirectory: "ssh://soft/stuff.git",
			},
			stubFunction: runCmdInDirFalseNoErr,
			wantOutput:   false,
			wantErr:      nil,
		},
		"IsShallowClone": {
			repo: Repository{
				Command:         []string{"git"},
				RemoteDir:       "./unbundle",
				TargetDirectory: "ssh://soft/stuff.git",
			},
			stubFunction: runCmdInDirTrueNoErr,
			wantOutput:   true,
			wantErr:      nil,
		},
	}

	for key, tc := range testCases {
		runCommandInDir = tc.stubFunction
		out, err := tc.repo.IsShallowClone()
		assert.Equal(t, fmt.Sprint(err), fmt.Sprint(tc.wantErr), "Error mismatch, Test Case: "+key)
		assert.Equal(t, fmt.Sprint(out), fmt.Sprint(tc.wantOutput), "Error mismatch, Test Case: "+key)
	}
}

func TestAddRemote(t *testing.T) {
	testCases := map[string]struct {
		repo         Repository
		stubFunction func(command []string, dir string) (string, error)
		wantOutput   string
		wantErr      error
	}{
		"HappyPath": {
			repo: Repository{
				Command:         []string{"git"},
				RemoteDir:       "./unbundle",
				TargetDirectory: "ssh://soft/stuff.git",
			},
			stubFunction: runCmdInDirEmptyOutputNoErr,
			wantOutput:   "",
			wantErr:      nil,
		},
	}

	for key, tc := range testCases {
		runCommandInDir = tc.stubFunction
		out, err := tc.repo.AddRemote()
		assert.Equal(t, fmt.Sprint(tc.wantErr), fmt.Sprint(err), "Error mismatch, Test Case: "+key)
		assert.Equal(t, fmt.Sprint(tc.wantOutput), fmt.Sprint(out), "Error mismatch, Test Case: "+key)
	}
}

func TestPushToRemote(t *testing.T) {
	testCases := map[string]struct {
		repo         Repository
		stubFunction func(command []string, dir string) (string, error)
		wantOutput   string
		wantErr      error
	}{
		"HappyPath": {
			repo: Repository{
				Command:         []string{"git"},
				RemoteDir:       "./unbundle",
				TargetDirectory: "ssh://soft/stuff.git",
			},
			stubFunction: runCmdInDirEmptyOutputNoErr,
			wantOutput:   "",
			wantErr:      nil,
		},
	}

	for key, tc := range testCases {
		runCommandInDir = tc.stubFunction
		out, err := tc.repo.PushAllBranchesToRemote()
		assert.Equal(t, fmt.Sprint(tc.wantErr), fmt.Sprint(err), "Error mismatch, Test Case: "+key)
		assert.Equal(t, fmt.Sprint(tc.wantOutput), fmt.Sprint(out), "Error mismatch, Test Case: "+key)
	}
}

func TestCheckForTag(t *testing.T) {
	testCases := map[string]struct {
		repo         Repository
		stubFunction func(command []string, dir string) (string, error)
		wantOutput   string
		wantErr      error
	}{
		"HappyPath": {
			repo: Repository{
				Command:         []string{"git"},
				RemoteDir:       "./unbundle",
				TargetDirectory: "ssh://soft/stuff.git",
			},
			stubFunction: runCmdInDirEmptyOutputNoErr,
			wantOutput:   "",
			wantErr:      nil,
		},
	}

	for key, tc := range testCases {
		runCommandInDir = tc.stubFunction
		out, err := tc.repo.CheckForTag()
		assert.Equal(t, fmt.Sprint(tc.wantErr), fmt.Sprint(err), "Error mismatch, Test Case: "+key)
		assert.Equal(t, fmt.Sprint(tc.wantOutput), fmt.Sprint(out), "Error mismatch, Test Case: "+key)
	}
}

func TestCheckForBranch(t *testing.T) {
	testCases := map[string]struct {
		repo         Repository
		stubFunction func(command []string, dir string) (string, error)
		wantOutput   string
		wantErr      error
	}{
		"HappyPath": {
			repo: Repository{
				Command:         []string{"git"},
				RemoteDir:       "./unbundle",
				TargetDirectory: "ssh://soft/stuff.git",
			},
			stubFunction: runCmdInDirEmptyOutputNoErr,
			wantOutput:   "false",
			wantErr:      nil,
		},
	}

	for key, tc := range testCases {
		runCommandInDir = tc.stubFunction
		out, err := tc.repo.CheckForBranch()
		assert.Equal(t, fmt.Sprint(tc.wantErr), fmt.Sprint(err), "Error mismatch, Test Case: "+key)
		assert.Equal(t, fmt.Sprint(tc.wantOutput), fmt.Sprint(out), "Error mismatch, Test Case: "+key)
	}
}

func TestPushTag(t *testing.T) {
	testCases := map[string]struct {
		repo         Repository
		stubFunction func(command []string, dir string) (string, error)
		wantOutput   string
		wantErr      error
	}{
		"HappyPath": {
			repo: Repository{
				Command:         []string{"git"},
				RemoteDir:       "./unbundle",
				TargetDirectory: "ssh://soft/stuff.git",
			},
			stubFunction: runCmdInDirEmptyOutputNoErr,
			wantOutput:   "",
			wantErr:      nil,
		},
	}

	for key, tc := range testCases {
		runCommandInDir = tc.stubFunction
		out, err := tc.repo.PushTag()
		assert.Equal(t, fmt.Sprint(tc.wantErr), fmt.Sprint(err), "Error mismatch, Test Case: "+key)
		assert.Equal(t, fmt.Sprint(tc.wantOutput), fmt.Sprint(out), "Error mismatch, Test Case: "+key)
	}
}

func TestPushBranch(t *testing.T) {
	testCases := map[string]struct {
		repo         Repository
		stubFunction func(command []string, dir string) (string, error)
		wantOutput   string
		wantErr      error
	}{
		"HappyPath": {
			repo: Repository{
				Command:         []string{"git"},
				RemoteDir:       "./unbundle",
				TargetDirectory: "ssh://soft/stuff.git",
			},
			stubFunction: runCmdInDirEmptyOutputNoErr,
			wantOutput:   "",
			wantErr:      nil,
		},
	}

	for key, tc := range testCases {
		runCommandInDir = tc.stubFunction
		out, err := tc.repo.PushBranch()
		assert.Equal(t, fmt.Sprint(tc.wantErr), fmt.Sprint(err), "Error mismatch, Test Case: "+key)
		assert.Equal(t, fmt.Sprint(tc.wantOutput), fmt.Sprint(out), "Error mismatch, Test Case: "+key)
	}
}

func TestPushVersionToRemote(t *testing.T) {
	testCases := map[string]struct {
		repo         Repository
		stubFunction func(command []string, dir string) (string, error)
		wantOutput   string
		wantErr      error
	}{
		"HappyPath": {
			repo: Repository{
				Command:         []string{"git"},
				RemoteDir:       "./unbundle",
				TargetDirectory: "ssh://soft/stuff.git",
			},
			stubFunction: runCmdInDirEmptyOutputNoErr,
			wantOutput:   "",
			wantErr:      nil,
		},
	}

	for key, tc := range testCases {
		runCommandInDir = tc.stubFunction
		out, err := tc.repo.PushVersionToRemote()
		assert.Equal(t, fmt.Sprint(tc.wantErr), fmt.Sprint(err), "Error mismatch, Test Case: "+key)
		assert.Equal(t, fmt.Sprint(tc.wantOutput), fmt.Sprint(out), "Error mismatch, Test Case: "+key)
	}
}

func TestPushAllBranchesToRemote(t *testing.T) {
	testCases := map[string]struct {
		repo         Repository
		stubFunction func(command []string, dir string) (string, error)
		wantOutput   string
		wantErr      error
	}{
		"HappyPath": {
			repo: Repository{
				Command:         []string{"git"},
				RemoteDir:       "./unbundle",
				TargetDirectory: "ssh://soft/stuff.git",
			},
			stubFunction: runCmdInDirEmptyOutputNoErr,
			wantOutput:   "",
			wantErr:      nil,
		},
	}

	for key, tc := range testCases {
		runCommandInDir = tc.stubFunction
		out, err := tc.repo.PushAllBranchesToRemote()
		assert.Equal(t, fmt.Sprint(tc.wantErr), fmt.Sprint(err), "Error mismatch, Test Case: "+key)
		assert.Equal(t, fmt.Sprint(tc.wantOutput), fmt.Sprint(out), "Error mismatch, Test Case: "+key)
	}
}

func TestRemoveTgtDir(t *testing.T) {
	testCases := map[string]struct {
		repo         Repository
		stubFunction func(command []string, dir string) (string, error)
		wantOutput   string
		wantErr      error
	}{
		"HappyPath": {
			repo: Repository{
				Command:         []string{"git"},
				RemoteDir:       "./unbundle",
				TargetDirectory: "ssh://soft/stuff.git",
			},
			stubFunction: runCmdInDirEmptyOutputNoErr,
			wantOutput:   "",
			wantErr:      nil,
		},
	}

	for key, tc := range testCases {
		runCommandInDir = tc.stubFunction
		err := tc.repo.CleanUp()
		assert.Equal(t, fmt.Sprint(tc.wantErr), fmt.Sprint(err), "Error mismatch, Test Case: "+key)
	}
}
