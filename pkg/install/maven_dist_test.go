/*
 *  File: pypi_dist.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/hoppr/droppr/pkg/configs"
	"gitlab.com/hoppr/droppr/pkg/nexus"
)

func TestMavenInstallNexus(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		uploadRc  []int
		openError []bool
		expected  Result
	}{
		"good": {
			[]int{200, 200},
			[]bool{false, false},
			Result{true, placeholderComp, ""},
		},
		"upload 1 failure": {
			[]int{400, 200},
			[]bool{false, false},
			Result{false, placeholderComp, "Error response from upload HTTP call: 400 Bad Request"},
		},
		"read 1 error": {
			[]int{200, 200},
			[]bool{true, false},
			Result{false, placeholderComp, "Mock Open Error"},
		},
		"upload 2 failure": {
			[]int{200, 400},
			[]bool{false, false},
			Result{false, placeholderComp, "Error response from upload HTTP call: 400 Bad Request"},
		},
		"read 2 error": {
			[]int{200, 200},
			[]bool{false, true},
			Result{false, placeholderComp, "Mock Open Error"},
		},
	}

	for key, tc := range testCases {
		uploadIndex := 0
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.uploadRc[uploadIndex])
			uploadIndex++
		}))
		defer server.Close()

		dist := mavenDist{*NewBaseDist("maven", testConfig("nexus", server.URL), "basedirectory")}
		comp := testComponent("pkg:maven/name.space/testMaven@1.2.3")

		testNexus := nexus.Server{Url: server.URL}

		openIndex := 0
		mockOpen := func(name string) (io.Reader, error) {
			if tc.openError[openIndex] {
				return bytes.NewReader([]byte("")), fmt.Errorf("Mock Open Error")
			}
			openIndex++
			return bytes.NewReader([]byte("Data from file " + name)), nil
		}

		result := dist.installNexus(comp, testNexus, "test_maven_repo", mockOpen)

		assert.Equal(t, tc.expected.Success, result.Success, "Result Status mis-match, Test Case: "+key)
		assert.Equal(t, comp, result.Component, "Component mis-match, Test Case: "+key)
		assert.Equal(t, tc.expected.Message, result.Message, "Message mis-match, Test Case: "+key)
	}
}

func TestMavenCheckNexusRepository(t *testing.T) {
	testCases := map[string]struct {
		getRepoRc    int
		getRepoJson  string
		createRepoRc int
		expected     error
	}{
		"got repo": {
			200,
			"[{\"name\": \"test-maven-repo\", \"format\": \"maven2\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
		"invalid repo": {
			200,
			"[{\"name\": \"test-maven-repo\", \"format\": \"pypi\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Repository test-maven-repo has format 'pypi', format 'maven2' requested"),
		},
		"create repo": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"maven2\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
		"get repo failure": {
			401,
			"[{\"name\": \"good-repo\", \"format\": \"maven2\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Error response from HTTP call: 401 Unauthorized"),
		},
		"create repo failure": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"maven2\", \"type\": \"hosted\"}]",
			401,
			fmt.Errorf("Error response from HTTP call to build repository: 401 Unauthorized"),
		},
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasSuffix(fmt.Sprint(r.URL), "/hosted") {
				w.WriteHeader(tc.createRepoRc)
			} else {
				w.WriteHeader(tc.getRepoRc)
				if _, err := w.Write([]byte(tc.getRepoJson)); err != nil {
					fmt.Printf("Error while writing RepoJson, %s", err)
				}
			}
		}))
		defer server.Close()

		dist := mavenDist{*NewBaseDist("maven", testConfig("nexus", server.URL), "basedirectory")}

		testNexus := nexus.Server{Url: server.URL}

		err := dist.CheckNexusRepository(testNexus, "test-maven-repo")

		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}

func TestMavenInstallLocal(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		config   *configs.DropprConfig
		testCmd  string
		expected Result
	}{
		"good": {
			testConfig("localinstall", ""),
			"echo",
			Result{true, placeholderComp, ""},
		},
		"config repo error": {
			&configs.DropprConfig{},
			"echo",
			Result{false, placeholderComp, "Error installing pkg:maven/name.space/testMaven@1.2.3 component locally: Unable to locate configuration for Purl type maven matching repository http://my-repo"},
		},
		"install failure pom": {
			testConfig("localinstall", ""),
			"ls",
			Result{false, placeholderComp, "exit status 2: ls: invalid option -- 'e'\nTry 'ls --help' for more information."},
		},
		"install failure no-pom": {
			testConfig("localinstall", ""),
			"ls",
			Result{false, placeholderComp, "exit status 2: ls: invalid option -- 'e'\nTry 'ls --help' for more information."},
		},
	}

	for key, tc := range testCases {
		for idx := range tc.config.Repos {
			tc.config.Repos[idx].Local_Install.Package_Manager_Command = []string{tc.testCmd}
		}

		dist := mavenDist{*NewBaseDist("maven", tc.config, "basedirectory")}
		comp := testComponent("pkg:maven/name.space/testMaven@1.2.3")

		result := dist.installLocal(comp)

		assert.Equal(t, tc.expected.Success, result.Success, "Result Status mis-match, Test Case: "+key)
		assert.Equal(t, comp, result.Component, "Component mis-match, Test Case: "+key)
		assert.Equal(t, tc.expected.Message, result.Message, "Message mis-match, Test Case: "+key)
	}
}
