/*
 *  File: helm_dist.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	cdx "github.com/CycloneDX/cyclonedx-go"
	"helm.sh/helm/v3/pkg/pusher"
	"helm.sh/helm/v3/pkg/registry"

	"gitlab.com/hoppr/droppr/pkg/configs"
	"gitlab.com/hoppr/droppr/pkg/extractors"
	"gitlab.com/hoppr/droppr/pkg/nexus"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

type helmDist struct {
	baseDist
}

type manifest struct {
	Manifests []struct {
		MediaType string `json:"mediaType"`
		Digest    string `json:"digest"`
		Size      int    `json:"size"`
	} `json:"manifests"`
}

type layers struct {
	Layers []struct {
		MediaType string `json:"mediaType"`
		Digest    string `json:"digest"`
		Size      int    `json:"size"`
	} `json:"layers"`
}

// no install local for helm

// func (self *helmDist) InstallFilesys(comp cdx.Component) Result {
// 	self.Log().Println("Processing file system install for helm files")
// 	return self.baseDist.InstallFilesys(comp)
// }

func (self *helmDist) InstallNexus(comp cdx.Component) Result {
	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to Nexus: %s", comp.PackageURL, err)
		self.Log().Errorf(msg)
		return Result{false, comp, msg}
	}

	repoName := repoConfig.Nexus.Repo_Name
	if repoName == "" {
		repoName = "droppr_helm"
	}

	targetNexus := nexus.Server{
		Url:      repoConfig.Nexus.Api_Url,
		Username: repoConfig.Username,
		Password: repoConfig.Password,
	}

	err = self.CheckNexusRepository(targetNexus, repoName)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return self.installNexus(comp, targetNexus, repoName, os.ReadDir, utils.OpenFileAsReader)
}

func (self *helmDist) installNexus(
	comp cdx.Component,
	targetNexus nexus.Server,
	repoName string,
	readDirFunc func(name string) ([]os.DirEntry, error),
	openFunc func(name string) (io.Reader, error),
) Result {

	purl := ParsePurl(comp.PackageURL)
	fileName := purl.Name
	data := map[string]interface{}{}
	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]
	files, err := readDirFunc(dir)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	for _, f := range files {
		fptr, err := openFunc(dir + string(os.PathSeparator) + f.Name())
		if err != nil {
			return Result{false, comp, err.Error()}
		}

		data["helm.asset1"] = fileName
		err = targetNexus.Upload(f.Name(), "helm.asset1", fptr, repoName, data)
		if err != nil {
			return Result{false, comp, err.Error()}
		}
	}

	return Result{true, comp, ""}
}

func (self *helmDist) CheckNexusRepository(targetNexus nexus.Server, repoName string) error {
	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()

	repo, err := targetNexus.GetRepository(repoName)
	if err != nil {
		return err
	}

	additionalParams := map[string]interface{}{
		"component": map[string]bool{
			"proprietaryComponents": true,
		},
	}

	if repo != nil {
		err := self.ValidateNexusRepository(repo, "helm", additionalParams)
		if err != nil {
			return err
		}
	} else {
		err := targetNexus.CreateRepository(repoName, "helm", "helm", additionalParams)
		if err != nil {
			return err
		}
	}

	return nil
}

func (self *helmDist) InstallOciRegistry(comp cdx.Component) Result {

	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		return Result{false, comp, "Error Getting Repo Config " + err.Error()}
	}

	myPusher, err := pusher.NewOCIPusher()
	if err != nil {
		return Result{false, comp, "Error Getting NewOCIPusher " + err.Error()}
	}

	myClient, err := registry.NewClient()
	if err != nil {
		return Result{false, comp, "Error Creating  Repo Client for Authentication " + err.Error()}
	}

	return self.installOciRegistry(comp, repoConfig, os.ReadDir, myPusher.Push, myClient.Login)

}

func (self *helmDist) installOciRegistry(
	comp cdx.Component,
	repoConfig *configs.PackageType,
	readDirFunc func(name string) ([]fs.DirEntry, error),
	ociPusherFunc func(chartRef string, url string, options ...pusher.Option) error,
	helmLoginFunc func(host string, options ...registry.LoginOption) error,
) Result {

	my_path := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]

	files, err := readDirFunc(my_path)
	if err != nil {
		return Result{false, comp, "Error Opening Temp Bundle Directory " + err.Error()}
	}
	my_files := []string{}
	for _, f := range files {
		if strings.HasPrefix(comp.PackageURL, "pkg:oci/") {
			if strings.HasSuffix(comp.PackageURL, f.Name()) {
				my_files = append(my_files, my_path+string(os.PathSeparator)+f.Name())
			}
		} else {
			my_file := my_path + string(os.PathSeparator) + f.Name()
			my_files = append(my_files, my_file)
		}
	}

	if (repoConfig.Username != "") && (repoConfig.Password != "") {

		//The host in the droppr config is *likely* an oci:// target. The registry login is going to puke on that...
		// ...so we need to strip that off if it's present here.
		repoLoginUrl := strings.TrimPrefix(repoConfig.OCI_Registry.Host, "oci://")

		myLoginOptions := registry.LoginOptBasicAuth(repoConfig.Username, repoConfig.Password)

		err := helmLoginFunc(repoLoginUrl, myLoginOptions)
		if err != nil {
			return Result{false, comp, "Error Logging into Repo Client " + err.Error()}
		}
	}

	repoUrl := repoConfig.OCI_Registry.Host + "/" + repoConfig.OCI_Registry.Project

	for _, f := range my_files {
		var err error
		if strings.HasSuffix(f, ".tgz") {
			err = ociPusherFunc(f, repoUrl)
		} else {
			newFilename := f + ".tar"
			_ = os.Rename(f, newFilename)

			extractedDir := filepath.Dir(f)

			_ = extractors.UnpackHopprArtifact(extractedDir, newFilename)

			_ = os.Rename(newFilename, f)
			_ = os.Remove(extractedDir + "/oci-layout")

			fileContents, _ := os.ReadFile(extractedDir + "/index.json")
			ociIndex := manifest{}
			_ = json.Unmarshal(fileContents, &ociIndex)

			digest := strings.Split(ociIndex.Manifests[0].Digest, ":")[1]

			fileContents, _ = os.ReadFile(extractedDir + "/blobs/sha256/" + digest)
			layers := layers{}
			_ = json.Unmarshal(fileContents, &layers)

			layer := strings.Split(layers.Layers[0].Digest, ":")[1]

			_ = os.Rename((extractedDir + "/blobs/sha256/" + layer), (extractedDir + "/" + layer))

			os.RemoveAll(extractedDir + "/blobs")
			os.Remove(extractedDir + "/index.json")

			err = ociPusherFunc((extractedDir + "/" + layer), repoUrl)
			os.Remove(extractedDir + "/" + layer)
		}

		if err != nil {
			return Result{false, comp, "Error Pushing " + f + " to " + repoUrl + " " + err.Error()}
		}

	}

	return Result{true, comp, ""}
}
