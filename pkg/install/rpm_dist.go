/*
 *  File: rpm_dist.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"regexp"
	"strings"

	cdx "github.com/CycloneDX/cyclonedx-go"

	"gitlab.com/hoppr/droppr/pkg/nexus"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

type rpmDist struct {
	baseDist
}

type yumRepoInfo struct {
	repoDepth float64
	targetDir string
}

var groupRepoMembers []string

func (self *rpmDist) InstallLocal(comp cdx.Component) Result {
	purl := ParsePurl(comp.PackageURL)

	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]
	file_list, err := os.ReadDir(dir)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	fqfn := dir + string(os.PathSeparator) + find_rpm_file(purl, file_list)

	return self.baseDist.installFileLocal(comp, self.buildCommand, fqfn)
}

func (self *rpmDist) buildCommand(fqfn string, comp cdx.Component) ([]string, error) {
	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		return nil, fmt.Errorf("Error installing %s component locally: %s", comp.PackageURL, err)
	}
	if len(repoConfig.Local_Install.Package_Manager_Command) == 0 || repoConfig.Local_Install.Package_Manager_Command[0] == "" {
		repoConfig.Local_Install.Package_Manager_Command = []string{"dnf"}
	}

	sudopath, err := exec.LookPath("sudo")
	command := []string{}

	if err == nil {
		self.Log().Printf("Prepending sudo to command as 'sudo' found in '%s'\n", sudopath)
		command = append(command, "sudo")
	}
	command = append(command, repoConfig.Local_Install.Package_Manager_Command...)
	command = append(command, "install", "-y", fqfn)
	return command, nil
}

func (self *rpmDist) InstallNexus(comp cdx.Component) Result {

	// Nexus install involves the following steps
	// 1. For each component to be uploaded to Nexus, the target repo with the right yum depth needs to be determind.
	// The helper function getRepoInfo determines the name, depth and directory structure of the target repository
	// 2. CheckNexusRepository creates a new repository if it has not been already created
	// 3. After the files get successfully uploaded, the yum folders with different data depths must be grouped together.
	// CheckNexusGroupRepository creates a new group /adds the repositories to the group

	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to Nexus: %s", comp.PackageURL, err)
		self.Log().Errorf(msg)
		return Result{false, comp, msg}
	}

	repoName := repoConfig.Nexus.Repo_Name
	if repoName == "" {
		repoName = "droppr_rpm"
	}

	collectiondir := getCollectionData(comp)[collectionDirectory]
	targetRepoInfo := getRepoInfo(collectiondir)
	repoNamewithDepth := repoName + "--d" + fmt.Sprint(targetRepoInfo.repoDepth)

	targetNexus := nexus.Server{
		Url:      repoConfig.Nexus.Api_Url,
		Username: repoConfig.Username,
		Password: repoConfig.Password,
	}

	err = self.CheckNexusRepository(targetNexus, repoNamewithDepth, targetRepoInfo.repoDepth)
	if err != nil {
		return Result{false, comp, err.Error()}
	}
	err = self.installNexus(comp, targetNexus, repoNamewithDepth, os.ReadDir, utils.OpenFileAsReader, targetRepoInfo.targetDir)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	repoAddedToGroup := addRepoToGroup(repoNamewithDepth)
	if repoAddedToGroup {
		err = self.CheckNexusGroupRepository(targetNexus, repoName)
		if err != nil {
			return Result{false, comp, err.Error()}
		}
	}

	return Result{true, comp, ""}

}

func addRepoToGroup(
	repoNamewithDepth string) bool {
	for _, elem := range groupRepoMembers {
		if elem == repoNamewithDepth {
			return false
		}
	}
	groupRepoMembers = append(groupRepoMembers, repoNamewithDepth)
	return true

}

func (self *rpmDist) installNexus(
	comp cdx.Component,
	targetNexus nexus.Server,
	repoName string,
	readDirFunc func(name string) ([]os.DirEntry, error),
	openFunc func(name string) (io.Reader, error),
	tgt string,
) error {

	purl := ParsePurl(comp.PackageURL)

	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]

	file_list, err := readDirFunc(dir)
	if err != nil {
		return err
	}

	filename := find_rpm_file(purl, file_list)

	fptr, err := openFunc(dir + string(os.PathSeparator) + filename)
	if err != nil {
		return err
	}

	data := map[string]interface{}{
		"yum.asset.filename": filename,
		"yum.directory":      tgt,
	}

	err = targetNexus.Upload(filename, "yum.asset", fptr, repoName, data)
	if err != nil {
		return err
	}

	return nil
}

func getRepoInfo(collectiondir string) yumRepoInfo {
	//This helper function is used to determine the yum repo's depth,name and target directory

	// Check the output of the hoppr:collection:directory
	// Here are some of the examples
	// "rpm/http%3A%2F%2Fatl.mirrors.clouvider.net%2Frocky%2F8%2FBaseOS%2Fx86_64%2Fos/Packages/g"
	// "rpm/https%3A%2F%2Fnexus.global.lmco.com%2Frepository%2Fyum-rocky-proxy/8.6/AppStream/x86_64/os/Packages/g"

	rerepo := regexp.MustCompile(`^rpm/.*?/(.*)$`)
	matches := rerepo.FindStringSubmatch(collectiondir)
	yumInfo := &yumRepoInfo{}
	yumInfo.repoDepth = float64(0)
	yumInfo.targetDir = ""

	// Handle 0 depth repos by only splitting when there is a match
	if len(matches) != 0 {
		// Split the string based on %2F or / because the input string can have both formats
		combinedFolderInfo := matches[len(matches)-1]
		folders := regexp.MustCompile(`%2F|\/`).Split(combinedFolderInfo, -1)
		depth := len(folders)

		for d, folder := range folders {
			if folder == "Packages" || folder == "RPMS" {
				depth = d
			}
		}

		yumInfo.repoDepth = float64(depth)
		yumInfo.targetDir = strings.Join(folders[:], "/")
	}
	return *yumInfo
}

func (self *rpmDist) CheckNexusRepository(targetNexus nexus.Server, repoName string, depth float64) error {
	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()

	repo, err := targetNexus.GetRepository(repoName)

	if err != nil {
		return err
	}

	additionalParams := map[string]interface{}{
		"component": map[string]bool{
			"proprietaryComponents": true,
		},
		"yum": map[string]interface{}{
			"repodataDepth": depth,
			"deployPolicy":  "STRICT",
		},
	}

	if repo != nil {
		err := self.ValidateNexusRepository(repo, "yum", additionalParams)
		if err != nil {
			return err
		}
	} else {

		err = targetNexus.CreateRepository(repoName, "yum", "yum", additionalParams)
		if err != nil {
			return err
		}
	}

	return nil
}

func (self *rpmDist) CheckNexusGroupRepository(targetNexus nexus.Server, groupName string) error {
	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()
	group, err := targetNexus.GetRepository(groupName)
	if err != nil {
		return err
	}

	additionalParams := map[string]interface{}{
		"group": map[string]interface{}{
			"memberNames": groupRepoMembers,
		},
	}

	if group != nil {
		err = targetNexus.CreateGroupRepository(groupName, "yum", "yum", additionalParams, false)

	} else {
		err = targetNexus.CreateGroupRepository(groupName, "yum", "yum", additionalParams, true)
	}
	if err != nil {
		return err
	}

	return nil
}

func find_rpm_file(purl parsedPurl, files []os.DirEntry) string {
	// Finds the file in the specified list that most closely matches the expected file name for the given purl
	//
	// There does not seem to be a definitive mapping from a purl to the name of a file that DNF downloads,
	// although the mapping used for expected_fn is often correct, or very close.
	//
	// To match, the file name must start with the purl name.  Of files that match, the one with the most exact
	// charaters matching is the winner.  The assumption is that ONE of the files should be a good match.
	//
	// This algortithm may need to be modified in the future to deal with unexpected edge cases.

	arch := "noarch"
	if purl.Quatlifiers["arch"] != "" {
		arch = purl.Quatlifiers["arch"]
	}

	expected_fn := fmt.Sprintf("%s-%s.%s.rpm", purl.Name, purl.Version, arch)

	best_guess := ""
	best_score := 0

	for _, f := range files {
		if f.Name() == expected_fn {
			return expected_fn
		}

		score := 0

		name_index := strings.Index(f.Name(), purl.Name)
		if name_index < 0 {
			continue
		}
		if name_index == 0 {
			score++
		}

		length := len(f.Name()) - name_index
		if length > len(expected_fn) {
			length = len(expected_fn)
		}
		for i := len(purl.Name); i < length; i++ {
			if f.Name()[name_index+i] == expected_fn[i] {
				score++
			}
		}

		if score > best_score {
			best_guess = f.Name()
			best_score = score
		}
	}

	if best_score == 0 {
		panic("Unable to find RPM file, expected file name: " + expected_fn)
	}
	return best_guess
}
