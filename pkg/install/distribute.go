/*
 *  File: distribute.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"bufio"
	"fmt"
	"io"
	"io/fs"
	"net/url"
	"os"
	"regexp"
	"runtime/debug"
	"strings"

	cdx "github.com/CycloneDX/cyclonedx-go"
	log "github.com/sirupsen/logrus"

	"gitlab.com/hoppr/droppr/pkg/configs"
	"gitlab.com/hoppr/droppr/pkg/scanner"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

type parsedPurl struct {
	Purl        string
	Type        string
	Namespace   string
	Name        string
	Version     string
	Quatlifiers map[string]string
	Subpath     string
}

type Result struct {
	Success   bool
	Component cdx.Component
	Message   string
}

const colorReset = "\033[0m"
const colorRed = "\033[31m"
const colorGreen = "\033[32m"

func distribute(basedir string,
	config *configs.DropprConfig,
	rundata fs.File,
	bomReader func(string) *cdx.BOM,
	workerFunc func(string, *configs.DropprConfig, cdx.Component) Result,
) error {

	if err := checkHopprVersion(rundata); err != nil {
		fmt.Printf("%s%s%s\n", colorRed, err.Error(), colorReset)
		return err
	}

	bom := bomReader(basedir + "/generic/_metadata_/_delivered_bom.json")

	var todo chan cdx.Component = make(chan cdx.Component, len(*bom.Components))
	var results chan Result = make(chan Result, len(*bom.Components))

	for w := 0; w < config.Num_Workers; w++ {
		go startWorker(w, workerFunc, basedir, config, todo, results)
	}

	compCount := 0
	for _, comp := range *bom.Components {
		if comp.Scope != cdx.ScopeExcluded {
			todo <- comp
			compCount++
		}
	}
	close(todo)

	successCount := 0
	for idx := 1; idx <= compCount; idx++ {
		r := <-results
		msg := "failed"
		color := colorRed
		if r.Success {
			msg = "succeeded"
			color = colorGreen
			successCount++
		}
		if len(r.Message) > 0 {
			msg += ": " + r.Message
		}
		pct := int(float64(idx*100)/float64(compCount) + 0.5)
		fmt.Printf("   [%3d%%] %sComponent %s %s%s\t\n", pct, color, r.Component.PackageURL, msg, colorReset)
	}

	if successCount < compCount {
		msg := fmt.Sprintf("%d of %d components failed to distribute properly", (compCount - successCount), compCount)
		fmt.Printf("\n%s%s%s\n", colorRed, msg, colorReset)
		return fmt.Errorf(msg)
	}

	fmt.Printf("\n%sAll %d components successfully processed.%s\n", colorGreen, successCount, colorReset)
	return nil
}

func Distribute(basedir string, config *configs.DropprConfig) error {
	rundata, err := os.Open(basedir + "/generic/_metadata_/_run_data_")
	if err != nil {
		msg := fmt.Sprintf("Unable to open _run_data_ file form bundle: %s", err.Error())
		log.Error(msg)
		fmt.Printf("\n%s%s%s\n", colorRed, msg, colorReset)
		return err
	}
	defer rundata.Close()

	if config.Hoppr_Metadata_Directory != "" {
		log.Printf("Copying Hoppr metadata to %s\n", config.Hoppr_Metadata_Directory)
		err := utils.CopyDir(basedir+"/generic/_metadata_", config.Hoppr_Metadata_Directory)
		if err != nil {
			msg := fmt.Sprintf("Unable to copy Hoppr Metadata: %s", err.Error())
			log.Error(msg)
			fmt.Printf("\n%s%s%s\n", colorRed, msg, colorReset)
			return err
		}
		fmt.Printf("%sHoppr metadata successfully copied to %s%s\n", colorGreen, config.Hoppr_Metadata_Directory, colorReset)
	}

	return distribute(basedir, config, rundata, scanner.ReadBom, ProcessComponent)
}

func ProcessComponent(baseDir string, config *configs.DropprConfig, comp cdx.Component) (success Result) {
	dist := selectDistributor(baseDir, config, comp)
	if dist == nil {
		return Result{false, comp, "Unable to determine distributor"}
	}

	return processComponent(dist, comp)
}

func selectDistributor(baseDir string, config *configs.DropprConfig, comp cdx.Component) distributor {

	purl := ParsePurl(comp.PackageURL)

	var dist distributor
	basedist := NewBaseDist(purl.Type, config, baseDir)
	switch purl.Type {
	case "deb":
		dist = &aptDist{*basedist}
	case "docker":
		dist = &dockerDist{*basedist}
	case "generic":
		dist = &rawDist{*basedist}
	case "git", "gitlab":
		dist = &gitDist{*basedist}
	case "helm", "oci":
		dist = &helmDist{*basedist}
	case "maven":
		dist = &mavenDist{*basedist}
	case "npm":
		dist = &npmDist{*basedist}
	case "nuget":
		dist = &nugetDist{*basedist}
	case "pypi":
		dist = &pypiDist{*basedist}
	case "rpm":
		dist = &rpmDist{*basedist}
	default:
		log.Warnf("No distributor defined for purl type %s\n", purl.Type)
	}

	return dist
}

func processComponent(dist distributor, comp cdx.Component) (result Result) {

	dist.Log().Printf("----- Beginning processing of %s -----", comp.PackageURL)
	defer func() {
		if r := recover(); r != nil {
			msg := fmt.Sprintf("Unexpected error processing '%s': %s", comp.PackageURL, r)
			dist.Log().Error(msg)
			dist.Log().Errorf(string(debug.Stack()))
			result = Result{false, comp, msg}
		}
		dist.Log().Printf("Processing of %s complete, success=%t   %s\n", comp.PackageURL, result.Success, result.Message)
		dist.Log().Flush()
	}()

	repoConfig, err := dist.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error selecting repo for %s: %s", comp.PackageURL, err)
		dist.Log().Error(msg)
		return Result{false, comp, msg}
	}

	if repoConfig.IsTargetTypeFileSystem() {
		result = dist.InstallFilesys(comp)
	} else if repoConfig.IsTargetTypeNexus() {
		result = dist.InstallNexus(comp)
	} else if repoConfig.IsTargetTypeLocalInstall() {
		result = dist.InstallLocal(comp)
	} else if repoConfig.IsTargetTypeOciRegistry() {
		result = dist.InstallOciRegistry(comp)
	} else if repoConfig.IsTargetTypeGitRemote() {
		result = dist.InstallGitRemote(comp)
	} else {
		msg := fmt.Sprintf("Invalid target type in config: %s", repoConfig.Target_Type)
		dist.Log().Error(msg)
		result = Result{false, comp, msg}
	}

	return result
}

func startWorker(id int, process func(string, *configs.DropprConfig, cdx.Component) Result, basedir string, config *configs.DropprConfig, todo chan cdx.Component, results chan Result) {
	log.Printf("Starting worker #%d", id)

	for comp := range todo {
		log.Printf("Processing '%s' in worker %d", comp.PackageURL, id)
		results <- process(basedir, config, comp)
	}

	log.Printf("Worker #%d shutting down", id)
}

func checkHopprVersion(rundata io.Reader) error {
	required := "1.8.0"

	scanner := bufio.NewScanner(rundata)

	version := ""
	log.Printf("Run data from Hoppr Bundle:")
	for scanner.Scan() {
		line := scanner.Text()
		log.Printf("|    %s", line)
		if strings.HasPrefix(line, "Hoppr Version:") && version == "" {
			version = strings.Trim(strings.TrimPrefix(line, "Hoppr Version:"), " ")
		}
	}

	if version == "" {
		return fmt.Errorf("Unable to determine Hoppr version from _run_data_ file in bundle")
	}

	if !utils.VersionAtLeast(version, required) {
		return fmt.Errorf("Bundle built with Hoppr version %s, version %s required", version, required)
	}

	return nil
}

func ParsePurl(packageUrl string) parsedPurl {
	// Parse a purl into its component segments
	// Handles percent-encoding as specified in https://github.com/package-url/purl-spec/blob/master/PURL-SPECIFICATION.rst#character-encoding

	re := regexp.MustCompile(`^pkg\:(.*?)/(?:([^?#@]*)/)?([^?#@]*?)(?:@([^?#]+))?(?:\?(.*?))?(?:#(.*))?$`)
	matches := append(re.FindStringSubmatch(packageUrl), "", "", "", "", "", "", "")

	unescaped_matches := []string{}
	for _, m := range matches {
		// Using PathUnescape instead of QueryUnescape because spaces are not escaped in a purl
		unesc, _ := url.PathUnescape(m)
		unescaped_matches = append(unescaped_matches, unesc)
	}

	qualMap := map[string]string{}

	if len(matches[5]) > 0 {
		for _, q := range strings.Split(matches[5], "&") {
			sides := strings.Split(q, "=")
			if len(sides) == 2 {
				key := strings.ToLower(strings.Trim(sides[0], " "))
				value, _ := url.QueryUnescape(strings.Trim(sides[1], " "))
				qualMap[key] = value
			}
		}
	}

	return parsedPurl{packageUrl,
		matches[1],           // type
		unescaped_matches[2], // namespace
		unescaped_matches[3], // name
		unescaped_matches[4], // version
		qualMap,              // qualifiers
		unescaped_matches[6], // subpath
	}
}
