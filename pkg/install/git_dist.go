/*
 *  File: git_dist.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	cdx "github.com/CycloneDX/cyclonedx-go"

	"gitlab.com/hoppr/droppr/pkg/configs"
	"gitlab.com/hoppr/droppr/pkg/git"
	"gitlab.com/hoppr/droppr/pkg/nexus"
	"gitlab.com/hoppr/droppr/pkg/utils"
)

type gitDist struct {
	baseDist
}

func (self *gitDist) InstallLocal(comp cdx.Component) Result {
	return self.installLocal(comp, self.buildCommand, utils.CopyDir)
}

func (self *gitDist) InstallFilesys(comp cdx.Component) Result {
	return self.installLocal(comp, func(repoConfig *configs.PackageType) []string { return []string{} }, utils.CopyDir)
}

func (self *gitDist) InstallGitRemote(comp cdx.Component) Result {
	validationError := self.validateGitRemoteInputs(comp)
	if validationError != nil {
		return Result{false, comp, validationError.Error()}
	}
	return self.installGitRemote(comp, pushToRemote)
}

func (self *gitDist) installLocal(
	comp cdx.Component,
	buildCmdFunc func(repoConfig *configs.PackageType) []string,
	copyFunc func(string, string) error,
) Result {
	collectionData := getCollectionData(comp)

	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to local file system: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	var targetConfigDir string
	if repoConfig.IsTargetTypeFileSystem() {
		targetConfigDir = repoConfig.File_System.Target_Directory
		if targetConfigDir == "" {
			msg := fmt.Sprintf("file system install for %s requires target directory to be specified", repoConfig.Purl_Type)
			self.Log().Error(msg)
			return Result{false, comp, msg}
		}
	} else if repoConfig.IsTargetTypeLocalInstall() {
		targetConfigDir = repoConfig.Local_Install.Target_Directory
		if targetConfigDir == "" {
			msg := fmt.Sprintf("local install for %s requires target directory to be specified", repoConfig.Purl_Type)
			self.Log().Error(msg)
			return Result{false, comp, msg}
		}
	} else {
		msg := fmt.Sprintf("Invalid target type for this install method %s", repoConfig.Target_Type)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	gitDir := ParsePurl(comp.PackageURL).Name
	srcDir := self.baseDir + string(os.PathSeparator) + collectionData[collectionDirectory] + string(os.PathSeparator) + gitDir
	tgtDir := targetConfigDir + string(os.PathSeparator) + collectionData[collectionDirectory] + string(os.PathSeparator) + gitDir
	err = copyFunc(srcDir, tgtDir)
	if err != nil {
		msg := fmt.Sprintf("Unable to copy from %s to %s, %s.", srcDir, tgtDir, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	command := buildCmdFunc(repoConfig)

	if len(command) > 0 {
		output, err := utils.RunCommandIn(command, tgtDir)
		self.Log().Println("Command output:\n     " + strings.ReplaceAll(output, "\n", "\n     "))
		if err != nil {
			return Result{false, comp, err.Error()}
		}
	}

	return Result{true, comp, ""}
}

func (self *gitDist) validateGitRemoteInputs(comp cdx.Component) error {
	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to local file system: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return errors.New(msg)
	}

	if repoConfig.IsTargetTypeGitRemote() {
		if repoConfig.Git_Remote.Host == "" {
			msg := "Error: Required host param was not set in config file"
			self.Log().Error(msg)
			return errors.New(msg)
		}
	} else {
		msg := fmt.Sprintf("Invalid target type for this install method %s", repoConfig.Target_Type)
		self.Log().Error(msg)
		return errors.New(msg)
	}
	return nil
}

// put this function here instead of in the util class for testability
func pushToRemote(repo *git.Repository) error {
	isShallowGitClone, IsShallowCloneErr := repo.IsShallowClone()
	if IsShallowCloneErr != nil {
		return IsShallowCloneErr
	}

	if isShallowGitClone {
		return errors.New("your git repository is a shallow clone. this is not currently supported in droppr")
	}

	_, addRemoteErr := repo.AddRemote()
	if addRemoteErr != nil {
		return addRemoteErr
	}

	if repo.Version != "" {
		_, pushVersionToRemoteErr := repo.PushVersionToRemote()
		if pushVersionToRemoteErr != nil {
			return pushVersionToRemoteErr
		}
	} else {
		_, pushToRemoteErr := repo.PushAllBranchesToRemote()
		if pushToRemoteErr != nil {
			return pushToRemoteErr
		}
	}

	return nil
}

func (self *gitDist) installGitRemote(
	comp cdx.Component,
	pushFunc func(repo *git.Repository) error,
) Result {
	collectionData := getCollectionData(comp)
	repoConfig, _ := self.GetRepoConfig(comp)

	purl := ParsePurl(comp.PackageURL)
	gitDir := purl.Name
	srcDir := self.baseDir + string(os.PathSeparator) + collectionData[collectionDirectory] + string(os.PathSeparator) + gitDir

	gitNamespace := purl.Namespace
	// host could be ssh://soft or https://myusername:password@mygitserver.com
	GitRemoteDir := repoConfig.Git_Remote.Host + string(os.PathSeparator) + gitNamespace + string(os.PathSeparator) + gitDir

	gitRepo := new(git.Repository)
	gitRepo.TargetBaseDir = self.baseDir
	gitRepo.Version = comp.Version
	gitRepo.RemoteDir = GitRemoteDir
	gitRepo.TargetDirectory = srcDir
	gitRepo.Command = repoConfig.Git_Remote.Command

	// push to remote
	processGitRemoteErr := pushFunc(gitRepo)
	if processGitRemoteErr != nil {
		return Result{false, comp, processGitRemoteErr.Error()}
	}

	return Result{true, comp, ""}
}

func (self *gitDist) buildCommand(repoConfig *configs.PackageType) []string {
	if len(repoConfig.Local_Install.Package_Manager_Command) == 0 || repoConfig.Local_Install.Package_Manager_Command[0] == "" {
		repoConfig.Local_Install.Package_Manager_Command = []string{"git"}
	}

	command := append([]string{}, repoConfig.Local_Install.Package_Manager_Command...)
	command = append(command, "init")

	return command
}

func (self *gitDist) InstallNexus(comp cdx.Component) Result {
	repoConfig, err := self.GetRepoConfig(comp)
	if err != nil {
		msg := fmt.Sprintf("Error installing %s component to Nexus: %s", comp.PackageURL, err)
		self.Log().Error(msg)
		return Result{false, comp, msg}
	}

	repoName := repoConfig.Nexus.Repo_Name
	if repoName == "" {
		repoName = "droppr_git"
	}

	targetNexus := nexus.Server{
		Url:      repoConfig.Nexus.Api_Url,
		Username: repoConfig.Username,
		Password: repoConfig.Password,
	}

	err = self.CheckNexusRepository(targetNexus, repoName)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return self.installNexus(comp, targetNexus, repoConfig.Nexus.Repo_Name, utils.OpenFileAsReader)
}

func (self *gitDist) installNexus(
	comp cdx.Component,
	targetNexus nexus.Server,
	repoName string,
	openFunc func(name string) (io.Reader, error),
) Result {

	dir := self.baseDir + string(os.PathSeparator) + getCollectionData(comp)[collectionDirectory]

	uploadFile := func(path string, d os.DirEntry, _ error) error {
		if d.IsDir() {
			return nil
		}

		relPath, _ := filepath.Rel(self.baseDir, filepath.Dir(path))
		pathParts := strings.Split(relPath, string(os.PathSeparator))

		data := map[string]interface{}{}
		data["raw.directory"] = "/" + filepath.Join(pathParts[2:]...)
		data["raw.asset1.filename"] = d.Name()

		fptr, err := openFunc(path)
		if err != nil {
			return err
		}

		err = targetNexus.Upload(d.Name(), "raw.asset1", fptr, repoName, data)
		if err != nil {
			self.Log().Printf("Error uploading file '%s' to nexus: %s\n", path, err.Error())
		}

		return err
	}

	err := filepath.WalkDir(dir, uploadFile)
	if err != nil {
		return Result{false, comp, err.Error()}
	}

	return Result{true, comp, ""}
}

func (self *gitDist) CheckNexusRepository(targetNexus nexus.Server, repoName string) error {
	targetNexus.GetLock().Lock()
	defer targetNexus.GetLock().Unlock()

	repo, err := targetNexus.GetRepository(repoName)
	if err != nil {
		return err
	}

	additionalParams := map[string]interface{}{
		"raw": map[string]string{
			"contentDisposition": "ATTACHMENT",
		},
		"storage": map[string]interface{}{
			"strictContentTypeValidation": false,
		},
	}

	if repo != nil {
		err := self.ValidateNexusRepository(repo, "raw", additionalParams)
		if err != nil {
			return err
		}
	} else {
		err := targetNexus.CreateRepository(repoName, "raw", "raw", additionalParams)
		if err != nil {
			return err
		}
	}

	return nil
}
