/*
 *  File: npm_dist_test.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package install

import (
	"bytes"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"testing/fstest"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hoppr/droppr/pkg/nexus"
)

func TestNpmInstallFilesys(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		purlType     string
		stat_result  error
		mkdir_result error
		copy_result  error
		expected     Result
	}{
		"success": {"npm", nil, nil, nil, Result{true, placeholderComp, ""}},
		"purl-not-found": {"badPurl", nil, nil, nil,
			Result{false, placeholderComp, "Error (GetRepoConfig) installing pkg:badPurl/filename@1.2.3 component to local file system: Unable to locate configuration for Purl type badPurl matching repository http://my-repo"}},
		"bad stat": {"npm", fmt.Errorf("Stat failed"), nil, nil,
			Result{false, placeholderComp, "Error (Stat) installing pkg:npm/filename@1.2.3 component to local file system: Stat failed"}},
		"bad mkdir": {"npm", nil, fmt.Errorf("Mkdir failed"), nil,
			Result{false, placeholderComp, "Error (Mkdir) installing pkg:npm/filename@1.2.3 component to local file system: Mkdir failed"}},
		"bad-copy": {"npm", nil, nil, fmt.Errorf("Copy failed"),
			Result{false, placeholderComp, "Unable to copy from base_directory/type/http%3A%2F%2Fmy-repo/the/collection/dir/filename-1.2.3.tgz to ./loc9/type/http%3A%2F%2Fmy-repo/the/collection/dir/filename-1.2.3.tgz, Copy failed."}},
	}

	test_config := testConfig("filesys", "./loc")
	for key, tc := range testCases {
		mockStat := func(string) (os.FileInfo, error) {
			var fileInfo os.FileInfo
			fileInfo, _ = os.Stat(".") // anything
			return fileInfo, tc.stat_result
		}
		mockMkdir := func(string, fs.FileMode) error {
			return tc.mkdir_result
		}
		mockCopy := func(string, string) error {
			return tc.copy_result
		}

		component := testComponent("pkg:" + tc.purlType + "/filename@1.2.3")
		basedist := NewBaseDist(tc.purlType, test_config, "base_directory")
		dist := &npmDist{*basedist}
		actual := dist.installFilesys(component, mockStat, mockMkdir, mockCopy)
		tc.expected.Component = component

		assert.Equal(t, tc.expected, actual, "Error, Test Case: "+key)
	}
}

func TestNpmInstallNexus(t *testing.T) {
	placeholderComp := testComponent("testPurl")
	testCases := map[string]struct {
		fileNames []string
		preamble  string
		uploadRc  []int
		openError bool
		expected  Result
	}{

		// only one fileName is needed because there is no directory read

		"good": {
			[]string{"alpha_1.2.3.tgz"},
			"pkg:npm/name/space/",
			[]int{200},
			false,
			Result{true, placeholderComp, ""},
		},

		"good2": {
			[]string{"alpha_1.2.3.tgz"},
			"pkg:npm/name/space%2F",
			[]int{200},
			false,
			Result{true, placeholderComp, ""},
		},

		"upload failure": {
			[]string{"alpha_1.2.3.tgz"},
			"pkg:npm/name/space/",
			[]int{400},
			false,
			Result{false, placeholderComp, "Error response from upload HTTP call: 400 Bad Request"},
		},
		"read error": {
			[]string{"alpha_1.2.3.tgz"},
			"pkg:npm/name/space/",
			[]int{200},
			true,
			Result{false, placeholderComp, "Mock Open Error"},
		},
		"check repo error": {
			[]string{"alpha_1.2.3.tgz"},
			"pkg:npm/name/space/",
			[]int{400, 200, 200},
			false,
			Result{false, placeholderComp, "Error response from upload HTTP call: 400 Bad Request"},
		},
	}

	for key, tc := range testCases {
		serverCallIndex := 0
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(tc.uploadRc[serverCallIndex])
			serverCallIndex++
		}))
		defer server.Close()

		dist := npmDist{*NewBaseDist("npm", testConfig("nexus", server.URL), "basedirectory")}
		comp := testComponent(tc.preamble + tc.fileNames[0])

		testNexus := nexus.Server{Url: server.URL}

		mockFileSys := fstest.MapFS{}
		for _, name := range tc.fileNames {
			if strings.HasSuffix(name, "/") {
				mockFileSys[strings.TrimRight(name, "/")] = &fstest.MapFile{Mode: os.ModeDir}
			} else {
				mockFileSys[name] = &fstest.MapFile{}
			}
		}

		mockOpen := func(name string) (io.Reader, error) {
			if tc.openError {
				return bytes.NewReader([]byte("")), fmt.Errorf("Mock Open Error")
			}
			return bytes.NewReader([]byte("Data from file " + name)), nil
		}

		result := dist.installNexus(comp, testNexus, "test-npm-repo", mockOpen)

		assert.Equal(t, tc.expected.Success, result.Success, "Result Status mis-match, Test Case: "+key)
		assert.Equal(t, comp, result.Component, "Component mis-match, Test Case: "+key)
		assert.Equal(t, tc.expected.Message, result.Message, "Message mis-match, Test Case: "+key)
	}
}

func TestNpmCheckNexusRepository(t *testing.T) {
	testCases := map[string]struct {
		getRepoRc    int
		getRepoJson  string
		createRepoRc int
		expected     error
	}{
		"got repo": {
			200,
			"[{\"name\": \"test-npm-repo\", \"format\": \"npm\", \"type\": \"hosted\"}]",
			200,
			nil,
		},

		"invalid repo": {
			200,
			"[{\"name\": \"test-npm-repo\", \"format\": \"pypi\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Repository test-npm-repo has format 'pypi', format 'npm' requested"),
		},
		"create new repo": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"npm\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
		"get repo failure": {
			401,
			"[{\"name\": \"good-repo\", \"format\": \"npm\", \"type\": \"hosted\"}]",
			200,
			fmt.Errorf("Error response from HTTP call: 401 Unauthorized"),
		},
		"create repo failure": {
			404,
			"[{\"name\": \"good-repo\", \"format\": \"npm\", \"type\": \"hosted\"}]",
			401,
			fmt.Errorf("Error response from HTTP call to build repository: 401 Unauthorized"),
		},
		"validate repo error": {
			200,
			"[{\"name\": \"test-npm-repo\", \"format\": \"npm\", \"type\": \"hosted\"}]",
			200,
			nil,
		},
	}

	for key, tc := range testCases {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasSuffix(fmt.Sprint(r.URL), "/hosted") {
				w.WriteHeader(tc.createRepoRc)
			} else {
				w.WriteHeader(tc.getRepoRc)
				if _, err := w.Write([]byte(tc.getRepoJson)); err != nil {
					fmt.Printf("Error while writing RepoJson, %s", err)
				}
			}
		}))
		defer server.Close()

		dist := npmDist{*NewBaseDist("npm", testConfig("nexus", server.URL), "basedirectory")}

		testNexus := nexus.Server{Url: server.URL}

		err := dist.CheckNexusRepository(testNexus, "test-npm-repo")

		assert.Equal(t, fmt.Sprint(tc.expected), fmt.Sprint(err), "Error mis-match, Test Case: "+key)
	}
}
