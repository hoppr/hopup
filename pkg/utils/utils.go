/*
 *  File: utils.go
 *  Copyright © 2023 Lockheed Martin <open.source@lmco.com>
 *
 *  MIT License
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 */

package utils

import (
	"fmt"
	"io"
	"io/fs"
	"os"
	"os/exec"
	"path"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

func Delete_Empty(s []string) []string {
	r := []string{}
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}

func RemoveDuplicateValues(stringSlice []string) []string {
	keys := make(map[string]bool)
	list := []string{}

	// If the key(values of the slice) is not equal
	// to the already present value in new slice (list)
	// then we append it. else we jump on another element.
	for _, entry := range stringSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func copyFile(src, dst string,
	openFunc func(name string) (*os.File, error),
	createFunc func(name string) (*os.File, error),
	copyFunc func(dst io.Writer, src io.Reader) (int64, error),
	statFunc func(name string) (os.FileInfo, error),
	chmodFunc func(name string, mode os.FileMode) error,
) error {

	var err error
	var srcfd *os.File
	var dstfd *os.File
	var srcinfo os.FileInfo

	if srcfd, err = openFunc(src); err != nil {
		return err
	}
	defer srcfd.Close()

	if dstfd, err = createFunc(dst); err != nil {
		return err
	}
	defer dstfd.Close()

	if _, err = copyFunc(dstfd, srcfd); err != nil {
		return err
	}
	if srcinfo, err = statFunc(src); err != nil {
		return err
	}
	return chmodFunc(dst, srcinfo.Mode())
}

func CopyFile(src, dst string) error {
	return copyFile(src, dst, os.Open, os.Create, io.Copy, os.Stat, os.Chmod)
}

func copyDir(src string, dst string,
	statFunc func(name string) (os.FileInfo, error),
	mkdirFunc func(path string, perm os.FileMode) error,
	readDirFunc func(name string) ([]os.DirEntry, error),
	copyFileFunc func(src, dst string) error,
) error {
	var err error
	var fds []os.DirEntry
	var srcinfo os.FileInfo

	if srcinfo, err = statFunc(src); err != nil {
		return err
	}

	if err = mkdirFunc(dst, srcinfo.Mode()); err != nil {
		return err
	}

	if fds, err = readDirFunc(src); err != nil {
		return err
	}
	for _, fd := range fds {
		srcfp := path.Join(src, fd.Name())
		dstfp := path.Join(dst, fd.Name())

		if fd.IsDir() {
			if err2 := copyDir(srcfp, dstfp,
				statFunc, mkdirFunc, readDirFunc, copyFileFunc); err2 != nil {
				fmt.Println(err2)
				err = err2
			}
		} else {
			if err2 := copyFileFunc(srcfp, dstfp); err2 != nil {
				fmt.Println(err2)
				err = err2
			}
		}
	}
	return err
}

func CopyDir(src string, dst string) error {
	return copyDir(src, dst, os.Stat, os.MkdirAll, os.ReadDir, CopyFile)
}

func OpenFileAsReader(name string) (io.Reader, error) {
	// Opens a file and returns an io.Reader
	// Useful to allow unit testing.  Much easier to mock an io.Reader than an *io.File

	var r io.Reader
	r, err := os.Open(name)
	return r, err
}

func MergeMap(original, update map[string]interface{}) {
	for key, val := range update {
		origVal, ok := original[key]
		if reflect.ValueOf(val).Kind() == reflect.Map && ok && reflect.ValueOf(origVal).Kind() == reflect.Map {
			MergeMap(original[key].(map[string]interface{}), val.(map[string]interface{}))
		} else {
			original[key] = val
		}
	}
}

func VersionAtLeast(ver string, required string) bool {
	// Checks that a version string is at or after a specified required version
	// Comparison is done numerically on each component of the version, in turn
	// Within a version element, non-numeric characters, and any characters following a non-numeric character, are ignored.
	// So version "1.2-alpha4.3" is treated as "1.2.3"

	vString := append(strings.Split(ver, "."), "", "", "")
	reqString := strings.Split(required, ".")

	nondigits := regexp.MustCompile(`\D.*`)

	for index, rStr := range reqString {
		v, _ := strconv.Atoi("0" + nondigits.ReplaceAllString(vString[index], ""))
		r, _ := strconv.Atoi("0" + nondigits.ReplaceAllString(rStr, ""))
		if v > r {
			return true
		}
		if v < r {
			return false
		}
	}

	return true // versions are the same
}

func OpenLogfile(logfileName string) (*os.File, error) {
	return openLogfile(logfileName, os.OpenFile)

}

func openLogfile(logfileName string, openFile func(name string, flag int, perm fs.FileMode) (*os.File, error)) (*os.File, error) {
	logFile, err := openFile(logfileName, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return nil, err
	}
	return logFile, err
}

func RunCommandIn(command []string, dir string) (string, error) {
	if len(command) == 0 {
		return "", fmt.Errorf("No command specified")
	}
	cmd := exec.Command(command[0], command[1:]...)
	cmd.Dir = dir

	output, err := cmd.Output()
	if exerr, ok := err.(*exec.ExitError); ok {
		err = fmt.Errorf(err.Error() + ": " + strings.TrimRight(string(exerr.Stderr), "\n"))
	}

	return string(output), err
}

func RunCommand(command []string) (string, error) {
	return RunCommandIn(command, "")
}
