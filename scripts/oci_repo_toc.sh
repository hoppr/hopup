#!/usr/bin/env bash
# This script supports integration testing of OCI Registry installs.
#
# Lists the contents of the specified OCI registry repository via regctl, eliminating fields likely
# to change from one run to the next.

usage() { echo "Usage: $0 [-r {registryhost}] [-u {USERNAME}] [-p {PASSWORD}] [-h] [-t {enabled|disabled}]"  1>&2; }

while getopts "hr:u:p:t:" arg; do
    case "${arg}" in
        r)
            OPT_REG_HOST=${OPTARG}
            ;;
        u)
            OPT_USERNAME=${OPTARG}
            ;;
        p)
            OPT_PASSWD=${OPTARG}
            ;;
        t)
            OPT_TLS=${OPTARG}
            if [ "$OPT_TLS" != "disabled" ] && [ $OPT_TLS != "enabled" ] ; then
               usage
               echo "Invalid TLS option: $OPT_TLS"
               exit 1
            fi
            ;;
        h)
            usage
            exit 0
            ;;
        *)
            usage
            exit 1
            ;;
    esac
done

# Set defaults if options not given
: "${OPT_REG_HOST:=localhost:5000}"
: "${OPT_USERNAME:=admin}"
: "${OPT_PASSWD:=$OCI_PW}"
: "${OPT_PASSWD:=abc123}"
: "${OPT_TLS:=disabled}"

# set command fails for http host because of missing credentials
# this is catch 22 in version 0.7.0 - as login requires the set first
regctl registry set --tls=$OPT_TLS $OPT_REG_HOST
# if [ $? -ne 0 ]; then
#    echo "ERROR: Failed to set registry: $OPT_REG_HOST with TLS $OPT_TLS"
#    exit 1
# fi

regctl registry  login -u $OPT_USERNAME --pass $OPT_PASSWD  $OPT_REG_HOST
if [ $? -ne 0 ]; then
   echo "ERROR: Failed to login to registry: $OPT_REG_HOST. Check username/password"
   exit 1
fi


# get the list of repo entries
REPO_LISTING=$(regctl repo ls ${OPT_REG_HOST})
if [ $? -ne 0 ]; then
   echo "ERROR: Failed to list repostories in registry: $OPT_REG_HOST"
   exit 1
fi

repoarray=($REPO_LISTING)


for repo in "${repoarray[@]}"; do
   image=${OPT_REG_HOST}/${repo}
   # get the tag(s) for this repo
   tag_listing=$(regctl tag ls ${image})
   tagarray=($tag_listing)

   # for each image:tag, get the manifest
   for tag in "${tagarray[@]}"; do
      #Helm charts in the repo will have some fields change run-to-run due to timestamping...
      #  ...so we'll filter the output of regclient to remove those fields.
      regctl image manifest --format '{{jsonPretty .}}' ${image}:${tag} | jq 'del(.config.digest,.annotations."org.opencontainers.image.created")'
#      echo ""
   done
#   echo ""
done
