NEXUS_WEB_PORT=8081
NEXUS_DOCKER_PORT=5000

docker run --volume  "$(pwd)":"$(pwd)" --workdir "$(pwd)" \
	-p $NEXUS_WEB_PORT:$NEXUS_WEB_PORT -p $NEXUS_DOCKER_PORT:$NEXUS_DOCKER_PORT \
	--detach  --name mytestnexus $NEXUS_IMAGE

rc=$?
if [ $rc != 0 ]
then
      echo ERROR: Failed to initiate Nexus, return code = $rc
      exit $rc
fi

docker ps
container=$(docker ps -lq --filter "name=nexus")
echo $container

export NEXUS_IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $container)
export NEXUS_HOST="$NEXUS_IP:$NEXUS_WEB_PORT"
echo "***** Found nexus host: $NEXUS_HOST"
sleep 20  ##### Waiting long enough for there to be a log file to grep

counter=0
while :
do
	counter=`expr $counter + 1`

	set +e
	nexus_up=$(docker exec $container grep "Started Sonatype Nexus OSS" /nexus-data/log/nexus.log | wc -l 2> /dev/null)
	set -e
	echo "Waiting for Nexus to start, attempt $counter, nexus_up = $nexus_up"
	if [[ "$nexus_up" != "0" ]]; then
		break
	fi

	if [ `expr $counter \> 30` == 1 ]; then
		echo ERROR: Waited too long for Nexus to start
		exit $counter
	fi

	sleep 10
done

export NEXUS_PW=$(docker exec $container cat /nexus-data/admin.password 2> /dev/null)
